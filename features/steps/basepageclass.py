#!/usr/local/bin/python3
from selenium import webdriver
import time
import sys
import os
from selenium.webdriver.common.keys import Keys

class basepageclass():
    """class to define basic page functions"""

    def __init__(self, browser='chrome'):
        self.browser = browser
        self.wd=''

    def set_browser(self):
        if self.browser == 'chrome':
            self.wd = webdriver.Chrome()
        return self.wd

    def get_browser(self):
       if set(self.wd):
           return wd
       else:
           return False

    def close(self):
        if self.wd:
            self.wd.quit()
        else:
            return False

    def open(self,page):
        self.wd.get(page)

    def login(self,url,email,passwd):
        login_message = "//*[@class='alert alert-info' and contains(.,'Signed in successfully.')]"
        self.wd.get(url)
        time.sleep(2)
        username = self.wd.find_element_by_id("user_email")
        password = self.wd.find_element_by_id("user_password")
        username.send_keys(email)
        password.send_keys(passwd)
        login = self.wd.find_element_by_xpath("//*[@type='submit']")
        login.submit()
        login_success = self.wd.find_element_by_xpath(login_message)
        self.wd.refresh()
        if login_success:
            return True
        else:
            return False

    def addtask(self,taskname):
        #mytasks_button = "//*[@role = 'button' and text()='My Tasks']"
        #mytasks = self.wd.find_element_by_xpath(mytasks_button)
        #mytasks.click()

        task_xpath = "//a[text() = \"" + taskname + "\"]"
        self.goto_mytasks()
        new_task = self.wd.find_element_by_id("new_task")
        new_task.send_keys(taskname)
        add_task = self.wd.find_element_by_xpath("//*[@ng-click = 'addTask()']")
        add_task.click()
        task = self.wd.find_element_by_xpath(task_xpath)
        if task:
            return True
        else:
            return False


    def taskExists(self,taskname):
        task_xpath = "//a[text() = \"" + taskname + "\"]"
        task = self.wd.find_element_by_xpath(task_xpath)
        if task:
            return True
        else:
            return False

    def addsubtask(self,subtask_name,due_date):
        manage_subtask_xpath = "(//*[@class=\"table\"]//*[@ng-click = \'editModal(task)\'])[1]"
        edit_task = self.wd.find_element_by_xpath(manage_subtask_xpath)
        edit_task.click()
        new_subtask = self.wd.find_element_by_id("new_sub_task")
        new_subtask.clear()
        new_subtask.send_keys(subtask_name)

        date = self.wd.find_element_by_id("dueDate")
        date.clear()
        date.send_keys(due_date)
        time.sleep(2)

        add_button = self.wd.find_element_by_id("add-subtask")
        add_button.click()

        close_managetask = self.wd.find_element_by_xpath("//*[@ng-click = 'close()']")
        close_managetask.click()
        self.wd.refresh()
        time.sleep(2)

        return True

    def subtaskExists(self,task_name,subtaskname):
        manage_subtask_xpath = "(//*[@class=\"table\"]//*[@ng-click = \'editModal(task)\'])[1]"
        subtask_xpath = "//a[text() = \'" + subtaskname + "\']"
        edit_task = self.wd.find_element_by_xpath(manage_subtask_xpath)
        edit_task.click()
        try:
            time.sleep(5)
            subtask = self.wd.find_element_by_xpath(subtask_xpath)
            if subtask:
                return True
            else:
                return False
        except:
            return False

    def check_element(self,elem):
        if elem == 'My Tasks':
            topbar_mytask_xpath = "//*[@class=\'nav navbar-nav\']//a[text() = \'My Tasks\']"
            mytasks = self.wd.find_element_by_xpath(topbar_mytask_xpath)
            if mytasks:
                return True
        return False

    def goto_mytasks(self):
        try:
            topbar_mytask_xpath = "//*[@class=\'nav navbar-nav\']//a[text() = \'My Tasks\']"
            mytasks = self.wd.find_element_by_xpath(topbar_mytask_xpath)
            mytasks.click()
            return True
        except:
            return False

    def check_greeting(self,text):
        try:
            greeting = "//h1[contains(., \"{}\")]".format(text)
            #greeting = "//h1[contains(., \"{}\")]".format('Vasudha')
            elem = self.wd.find_element_by_xpath(greeting)
            if elem:
                return True
        except:
            return False



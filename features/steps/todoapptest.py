from behave import *
import sys
from basepageclass import *
from time import sleep
from datetime import datetime, timedelta
import string

#define global variable used through out the tests
global browser, bc, login_success, task_name

#By default the tests open Chrome browser. But, tests can be extended to
#other browsers like Firefox.

@given('the browser is chrome')
def browser_setup(context):
    global browser, bc

    #Use existing browser session
    #otherwise create a new session
    try:
        browser
    except:
        bc = basepageclass('chrome')
        browser = bc.set_browser()
    if browser:
        assert True
    else:
        assert False

#Make sure user is logged in first
@given('logged into todo App as Vasudha')
def login(context):
    global login_success
    url="https://qa-test.avenuecode.com/users/sign_in"
    username = "vasudharalla@gmail.com"
    passwd = "actest14"

    #skip login if user is already logged in
    try:
        login_success
    except:
        login_success = bc.login(url,username,passwd)
    assert login_success is True


@then('My Tasks is in the top bar')
def check_mytasks(context):
    result = bc.check_element('My Tasks')
    assert result is not False

@when('clicked on My Tasks')
def click_mytasks(context):
    result = bc.goto_mytasks()
    assert result is not False

@then('displayed message should read "{text}"')
def check_display_message(context,text):
    result = bc.check_greeting(text)
    assert result is True

#Create new task.
#if task is already created, we skip adding new task.

@when('new task is created')
def add_newtask(context):
    global task_name
    #if a task is already added, skip this step
    try:
        task_name
        result = True
    except:
        task_name = "Task : "+ str(datetime.now())
        result = bc.addtask(task_name)
    sleep(2)
    assert result is not False

#verify task is created successfully
@then('task is created')
def verify_task(context):
    result = bc.taskExists(task_name)
    assert result is not False

#Add subtask to the task that was created in previous steps
@when('subtask is added to the task')
def add_subtask(context):
    global subtask_name
    dt = datetime.now()
    subtask_name = "Sub task : "+ str(dt)
    dt = dt + timedelta(days=30)
    due_date = str(dt.month) + "/" + str(dt.day) + "/" + str(dt.year)
    result = bc.addsubtask(subtask_name,due_date)
    sleep(2)
    assert result is not False

#verify subtask is created for the task created in previous steps
@then('sub task is created')
def add_subtask(context):
    try:
        result = bc.subtaskExists(task_name,subtask_name)
    except:
        result = False
    assert result is not False

@then('close the session')
def cleanup(context):
    try:
        bc.close()
        pass
    except:
        assert False
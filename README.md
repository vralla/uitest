To get started, we'll need to ensure that Behave is installed. The easiest means of doing so is with Pip:

pip install Behave

install Selenium:

pip install selenium

This tests ToDo App
I currently have following scenarios automated:

Scenario: ‘My Tasks’ link on the NavBar
Given the browser is chrome
and logged into todo App as Vasudha
Then My Tasks is in the top bar


Scenario: Check displayed message
Given the browser is chrome
and logged into todo App as Vasudha
when clicked on My Tasks
Then displayed message should read "Hey Vasudha, this is your todo list for today:"


Scenario: Add new task
Given the browser is chrome
and logged into todo App as Vasudha
when  new task is created
Then task is created


Scenario: Add sub task
Given the browser is chrome
and logged into todo App as Vasudha
when new task is created
and subtask is added to the task
Then sub task is created

Scenario: Cleanup
Given the browser is chrome
and logged into todo App as Vasudha
Then close the session


Running the test
From this repo, run the test by running the following command:
behave



